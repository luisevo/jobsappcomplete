export class Job {
  title: string;
  place: string;
  date: Date;
  salary?: number;
  type: string;
  area: string;
  description: string;
  requirements: string;
  company: string;
}
