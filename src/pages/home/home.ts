import {Component, ViewChild} from '@angular/core';
import {App, IonicPage, Nav, NavController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {TOKEN} from "../../utils/constants";
import {Storage} from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;

  rootPage = 'JobsPage';
  pages = [
    {title: 'Puestos', component: 'JobsPage'},
    {title: 'Perfil', component: 'ProfilePage'},
  ];
  constructor(
    public navCtrl: NavController,
    private auth: AuthProvider,
    private storage: Storage,
    ) {
  }

  goPage(component) {
    this.nav.setRoot(component)
  }

  ionViewCanEnter() {
    return !!this.auth.token;
  }

  logout() {
    this.storage.remove(TOKEN)
      .then(()=>{
        this.auth.token = null;
        this.navCtrl.setRoot('LoginPage')
      })
  }

}
