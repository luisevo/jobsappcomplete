import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Job} from "../../models/job.model";

@IonicPage()
@Component({
  selector: 'page-job-detail',
  templateUrl: 'job-detail.html',
})
export class JobDetailPage {
  job:Job;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.job = this.navParams.get('job')
  }

  ionViewDidLoad() {

  }

}
