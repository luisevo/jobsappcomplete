import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {Job} from "../../models/job.model";

@IonicPage()
@Component({
  selector: 'page-jobs',
  templateUrl: 'jobs.html',
})
export class JobsPage {
  jobs:Job[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public data: DataProvider
  ) {
  }

  ionViewDidLoad() {
    this.data.getJobs()
      .subscribe(
        result => this.jobs = result
      )
  }

  openDetail(job) {
    this.navCtrl.push('JobDetailPage', {job})
  }

}
