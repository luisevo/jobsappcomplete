import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Profile} from "../../models/profile.model";
import {DataProvider} from "../../providers/data/data";

@IonicPage()
@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {
  profile: Profile;
  form: FormGroup;
  avatars = [
    'avatar1',
    'avatar2',
    'avatar3',
    'avatar4',
    'avatar5',
    'avatar6',
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public data: DataProvider
  ) {
   this.form = this.formBuilder.group({
     name: ['', Validators.required],
     lastname: ['', Validators.required],
     avatar: ['', Validators.required],
     age: '',
   });
  }

  ionViewDidLoad() {
    this.profile = this.navParams.get('profile');
    if (this.profile) this.form.patchValue({
      name: this.profile.name,
      lastname: this.profile.lastname,
      avatar: this.profile.avatar,
      age: this.profile.age,
    })
  }

  save() {
    this.data.updateProfile(this.profile.id, this.form.value)
      .subscribe(
        result => this.navCtrl.pop()
      )
  }
}
