import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {Profile} from "../../models/profile.model";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile: Profile;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public data: DataProvider,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    this.load()
  }

  load(){
    this.data.getProfile()
      .subscribe(
        result => this.profile = result
      )
  }

  getAvatar(img) {
    return `assets/imgs/${img}.png`
  }

  edit() {
    let modal = this.modalCtrl.create('ProfileEditPage', {profile: this.profile});
    modal.present()
    modal.onDidDismiss(() => {
      this.load()
    })
  }

}
