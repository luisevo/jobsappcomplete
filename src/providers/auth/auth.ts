import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable()
export class AuthProvider {
  token: string;

  constructor(public http: HttpClient) {
  }

  signIn(body: {username, password}): Observable<any> {
    return this.http.post('/auth/', body);
  }
}
